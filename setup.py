from setuptools import setup 

setup(name='pathUrl',
      description='Tool for converting from path to URL or URL to path on Ifremer network',
      url='https://git.cersat.fr/oarcher/pathurl',
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      license='GPL',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      packages=['pathurl'],
      zip_safe=False,
      scripts=['bin/pathUrl.py'],
      package_data={'pathurl': ['etc/*']}
)
