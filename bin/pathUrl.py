#!/usr/bin/env python

import pathurl
import argparse
import sys

description = """Converts string from path to URL or from URL to path on Ifremer network"""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-u", "--toUrl", action="store", nargs="*", help="""Input string, containing a path to be converted into an Url.
If no string is given, the input string is taken on stdin (and can be multiple).
""")
parser.add_argument("-p", "--toPath", action="store", nargs="*", help="""Input string, containing an url to be converted into a path.
If no string is given, the input string is taken on stdin (and can be multiple). """)
parser.add_argument("-s", "--schema", action="store", default=pathurl.default_schema, help="Schema to use (will change which translations are used")
parser.add_argument("-r", "--relative", action="store", default=pathurl.default_relative, help="if possible, result will be a relative path from this path or url")

args = parser.parse_args()

if args.toUrl is not None:
    lines = []
    if len(args.toUrl) == 0:
        lines = sys.stdin.readlines()
    else:
        lines = args.toUrl
    for l in lines:
        print(pathurl.toUrl(l.strip(), schema=args.schema, relative=args.relative))

if args.toPath is not None:
    lines = []
    if len(args.toPath) == 0:
        lines = sys.stdin.readlines()
    else:
        lines = args.toPath
    for l in lines:
        print(pathurl.toPath(l.strip(), schema=args.schema,relative=args.relative))
